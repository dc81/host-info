## host-info

Simple node application to report host information such as hostname, available memory and network interfaces

Author: Joe Limwiwatkul

### Development 

Run the application 

```
node app.js
```

Tested with NodeJS 16

### Build docker container

Build container image

```
docker build -t registry.gitlab.com/dc81/host-info .
```

### Run container locally

```
docker run -d --name host-info -p 10001:10001 registry.gitlab.com/dc81/host-info 
```

### Update Docker registry

Push updated container image to Gitlab's Docker registry

```
docker push registry.gitlab.com/dc81/host-info
```