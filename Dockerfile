FROM node:16-alpine
RUN apk add --update curl && \
    rm -rf /var/cache/apk/*
ADD app.js /app.js
ENTRYPOINT ["node", "app.js"]
