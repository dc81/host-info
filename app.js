// host-info
// Author: Joe Limwiwatkul
// Simple node application to report host information
//
const os = require('os');
const http = require('http');
var PORT = 10001
console.log("host-info running at " + PORT);

// Get Hostname
var hostname = os.hostname()

// Get available memory (GB)
var freemem = (os.freemem() / (1024*1024*1024)).toFixed(2)
var totalmem = (os.totalmem() / (1024*1024*1024)).toFixed(2)

// Examine network interface
var addr4 = ""
var addr6 = ""
var interfaces = os.networkInterfaces();
for (var interface in interfaces) {
    if (interfaces[interface][0] !== undefined) {
        var address4 = interfaces[interface][0].address
        var netmask4 = interfaces[interface][0].netmask
        var addr4 = addr4 + "\n" + interface + " - " + address4 + "/" + netmask4
    }
    if (interfaces[interface][1] !== undefined) {
        var address6 = interfaces[interface][1].address
        var netmask6 = interfaces[interface][1].netmask
        var addr6 = addr6 + "\n" + interface + " - " + address6 + "/" + netmask6
    }
}

// Create HTTP handler
var http_handler = function(request, response) {
    console.log("Received request " + request.connection.remoteAddress);
    response.writeHead(200);
    response.write("Host name: " + hostname + "\n");
    response.write("Free / Total Memory: " + freemem + " (GB) / " + totalmem + " (GB)\n\n");
    response.write("IPv4 Network Interfaces: " + addr4 + "\n\n");
    response.write("IPv6 Network Interfaces: " + addr6);
    response.end();
};

// Create server listening on PORT
var www = http.createServer(http_handler);
www.listen(PORT);
